import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from "./components/products/products.component";
import { ProductsRoutingModule } from "./products-routing.module";
import { ProductModule } from "./components/products/product/product.module";
import { NzSelectModule } from "ng-zorro-antd/select";
import { FormsModule } from "@angular/forms";
import { FilterByPipe } from "../../shared/pipes/filter-by.pipe";

@NgModule({
  declarations: [
    ProductsComponent,
    FilterByPipe
  ],
  exports: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ProductModule,
    ProductsRoutingModule,
    NzSelectModule
  ]
})
export class ProductsModule { }
