import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IProductAdded, Product } from "../../../models/product";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent {
  @Input() product!: Product;
  @Output() productAdded = new EventEmitter<IProductAdded>();
  public selectedQuantity: number = 0;

  quantityFormatter = (value: number): string => `${value} / ${this.product?.quantity - this.product?.selectedQuantity}`;

  public addToCart(): void {
    if (this.selectedQuantity > 0) {
      this.productAdded.emit({
        productId: this.product.id,
        quantity: this.selectedQuantity
      });
      this.selectedQuantity = 0;
    }
  }
}
