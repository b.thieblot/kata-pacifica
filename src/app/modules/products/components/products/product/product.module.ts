import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from "./product.component";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzInputNumberModule } from "ng-zorro-antd/input-number";
import { FormsModule } from "@angular/forms";
import { NzSpaceModule } from "ng-zorro-antd/space";
import { NzButtonModule } from "ng-zorro-antd/button";

@NgModule({
  declarations: [
    ProductComponent
  ],
  exports: [
    ProductComponent
  ],
  imports: [
    CommonModule,
    NzCardModule,
    NzInputNumberModule,
    NzSpaceModule,
    FormsModule,
    NzButtonModule
  ]
})
export class ProductModule { }
