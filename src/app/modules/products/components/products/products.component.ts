import { Component } from '@angular/core';
import { ProductService } from "../../../../shared/services/product.service";
import { map, Observable } from "rxjs";
import { IProductAdded, Product, ProductCategory } from "../../models/product";
import { CartService } from "../../../../shared/services/cart.service";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent{
  public products$: Observable<Product[]>;
  public productCategory: ProductCategory[];
  public selectedCategories: ProductCategory[] = [];
  public productInCart: Map<number, number>;

  constructor(
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.products$ = combineLatest([
      this.productService.getProducts(),
      this.cartService.selectedProducts$.asObservable()
    ]).pipe(
      map(([products, selectedProducts]) => products.map(product => {
        const quantityInCart = selectedProducts.get(product.id) ?? 0;
        product.updateQuantitySelected(quantityInCart);
        return product;
      }))
    );

    this.productCategory = Object.values(ProductCategory);

    this.productInCart = this.cartService.selectedProducts;
  }

  public addToCart(item: IProductAdded): void {
    this.cartService.addNewProduct(item);
  }
}
