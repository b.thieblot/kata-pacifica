import { AppUtils } from "../../../shared/utils/app-utils";

export enum ProductCategory {
  Food = 'Food',
  Medecine = 'Medecine',
  Books= 'Books',
  Electric = 'Electric',
  Parfum = 'Parfum'
}

export interface IProductAdded {
  productId: number;
  quantity: number;
}

export interface IProduct {
  id: number;
  productName: string;
  price: number;
  quantity: number;
  isImported: boolean;
  category: ProductCategory
}

export class Product implements IProduct {
  id!: number;
  productName!: string;
  price!: number;
  quantity!: number;
  isImported!: boolean;
  category!: ProductCategory
  //
  public taxes: number;
  public taxedPrice: number;
  public selectedQuantity: number;

  constructor(data: IProduct) {
    let key: keyof IProduct;
    for (key in data) {
      switch (key) {
        case "id":
          this.id = data.id;
          break;
        case "productName":
          this.productName = data.productName === '' ? 'Nom non disponible' : data.productName;
          break;
        case "price":
          this.price = data.price;
          break;
        case "quantity":
          this.quantity = data.quantity;
          break;
        case "isImported":
          this.isImported = data.isImported;
          break;
        case "category":
          this.category = data.category;
          break;
      }
    }
    // End for in
    const baseTax = this.calculateTax() * this.price;
    this.taxes = this.roundedTax(baseTax);
    this.taxedPrice = AppUtils.roundedNumber(this.price + this.taxes, 2);
    //
    this.selectedQuantity = 0;
  }

  public updateQuantitySelected(quantity: number): this {
    this.selectedQuantity = quantity;
    return this;
  }

  private calculateTax(): number {
    const baseTax = (() => {
      switch (this.category) {
        case ProductCategory.Food:
        case ProductCategory.Medecine:
          return 0;
        case ProductCategory.Books:
          return 0.1;
        default:
          return 0.2;
      }
    })();

    return baseTax + (this.isImported ? 0.05 : 0);
  }

  /**
   * Function which round the tax value on a certain basis. Here, it's 0.05
   * Ex: 305.26 will be rounded to 305.3
   * @param taxValue
   * @private
   */
  private roundedTax(taxValue: number): number {
    const BASE_TAX = 0.05;
    const roundedTax = Math.ceil(taxValue / BASE_TAX) * BASE_TAX;
    return AppUtils.roundedNumber(roundedTax, 2);
  }
}
