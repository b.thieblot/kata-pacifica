import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from "./components/cart/cart.component";
import { CartRoutingModule } from "./cart-routing.module";
import { NzListModule } from "ng-zorro-antd/list";
import { NzIconModule } from "ng-zorro-antd/icon";

@NgModule({
  declarations: [
    CartComponent
  ],
  exports: [
    CartComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    NzListModule,
    NzIconModule
  ]
})
export class CartModule { }
