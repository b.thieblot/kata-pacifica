import { Component } from '@angular/core';
import { CartService } from "../../../../shared/services/cart.service";
import { combineLatest, map, Observable } from "rxjs";
import { Product } from "../../../products/models/product";
import { ProductService } from "../../../../shared/services/product.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent {
  public productList$!: Observable<Product[]>;
  public totalTaxes$: Observable<number>;
  public totalPrice$: Observable<number>;
  public hasProductsInCart$: Observable<boolean>;

  constructor(
    private productService: ProductService,
    private cartService: CartService
  ) {
    this.productList$ = combineLatest([
      this.productService.getProducts(),
      this.cartService.selectedProducts$.asObservable()
    ]).pipe(
      map(([products, selectedProducts]) => products
        .filter(product => selectedProducts.has(product.id))
        .map(product => product.updateQuantitySelected(selectedProducts.get(product.id) as number))
      )
    );

    this.totalTaxes$ = this.productList$.pipe(
      map(products => products.map(product => product.taxes * product.selectedQuantity).reduce((a, b) => a + b, 0))
    );

    this.totalPrice$ = this.productList$.pipe(
      map(products => products.map(product => product.taxedPrice * product.selectedQuantity).reduce((a, b) => a + b, 0))
    );

    this.hasProductsInCart$ = this.productList$.pipe(
      map(products => products?.length > 0)
    );
  }

  public removeProduct(productId: number): void {
    this.cartService.removeProduct(productId);
  }

}
