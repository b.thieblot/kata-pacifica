export class AppUtils {
  static roundedNumber(val: number, roundDecimals: number): number {
    return Math.round((val + Number.EPSILON) * (10**roundDecimals)) / (10**roundDecimals);
  }
}
