import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { IProduct } from "../../modules/products/models/product";

@Injectable({
  providedIn: 'root'
})
export class ProductApiService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getJsonProducts(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>('/assets/products.json');
  }
}
