import { Injectable } from '@angular/core';
import { ProductApiService } from "./product-api.service";
import { catchError, map, Observable, of } from "rxjs";
import { Product } from "../../modules/products/models/product";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private productApiService: ProductApiService
  ) { }

  public getProducts(): Observable<Product[]> {
    return this.productApiService.getJsonProducts().pipe(
      catchError(() => of([])),
      map(products => products ? products.map(product => new Product(product)) : [])
    );
  }
}
