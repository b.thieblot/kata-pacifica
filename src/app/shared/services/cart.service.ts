import { Injectable } from '@angular/core';
import { IProductAdded } from "../../modules/products/models/product";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public selectedProducts$ = new BehaviorSubject<Map<number, number>>(new Map<number, number>());

  constructor() { }

  get selectedProducts() {
    return this.selectedProducts$.getValue();
  }

  set selectedProducts(val: Map<number, number>) {
    this.selectedProducts$.next(val);
  }

  public addNewProduct({ productId, quantity }: IProductAdded) {
    const initialValue = this.selectedProducts;
    const currentQuantity = initialValue.get(productId) ?? 0;
    initialValue.set(productId, currentQuantity + quantity);
    this.selectedProducts = initialValue;
  }

  public removeProduct(productId: number): void {
    const initialValue = this.selectedProducts;
    initialValue.delete(productId);
    this.selectedProducts = initialValue;
  }
}
