import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {
  transform<T, TKey extends keyof T>(values: T[], key: TKey, filterValue: T[TKey][]): T[] {
    if (!values || !key || !filterValue) {
      return values;
    }
    if (filterValue.length === 0) {
      return values;
    }
    return values.filter(value => filterValue.includes(value[key]));
  }

}
