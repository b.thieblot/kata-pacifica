import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [{
  path: 'products',
  loadChildren: () => import('./modules/products/products.module').then(mod => mod.ProductsModule),
}, {
  path: 'cart',
  loadChildren: () => import('./modules/cart/cart.module').then(mod => mod.CartModule),
}, {
  path: '',
  redirectTo: 'products',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
